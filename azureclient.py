#!/usr/bin/env python

import json
import subprocess
from tempfile import TemporaryFile


class CLIExecutor(object):
    def __init__(self, debug_mode=False):
        self.__stderr = ""
        self.__stdout = ""
        self.__command = ""
        self.__debug_mode = debug_mode


    def execute(self, command, **kwargs):
        """
        It runs the run() method, but
        returns a tuple of:
            - result - True of false
            - 'json' or 'plain' depends on the above
            - stdout - a json object or plain text
            - stderr - check it if result is False
        """
        result = self.run(command, **kwargs)
        output = self.stdout if not self.is_json() else self.json
        return result, 'json' if self.is_json() else 'plain', output, self.stderr



    def run(self, command, **kwargs):
        """
        Parameters:
            - command - must be a list of a commant and its arguments, also splitted into pieces of the list
        Returns:
            - True if the command returned 0
            - False in other cases
        Updates:
            - self.stdout might be json or plain text, depends on output
            - self.stderr - with messages
        """
        result = False

        self.__debug_mode = kwargs.get("debug_mode") if kwargs.get("debug_mode", None) != None else self.__debug_mode
        if self.__debug_mode:
            # Dry run mode
            self.__stdout = kwargs.get("in_output")
            result = kwargs.get("result") if kwargs.get("result", None) != None else True
            self.__command = command
        else:
            # Production mode
            stderr = TemporaryFile() if kwargs.get("split_stderr", False) else subprocess.STDOUT
            try:
                cmd = command
                if kwargs.get("query_filter", None) != None:
                    cmd += [ "--query", kwargs.get("query_filter") ]
                self.__command = cmd
                self.__stdout = subprocess.check_output(cmd, stderr=stderr, shell=False).decode("utf-8")
            except subprocess.CalledProcessError as err:
                self.__stderr = err.output.decode("utf-8")
            else:
                result = True
            finally:
                if stderr != subprocess.STDOUT:
                    stderr.seek(0)
                    self.__stderr = stderr.read().decode("utf-8")
                    stderr.close()
            pass
        return result


    @property
    def command(self): return self.__command

    @property
    def stdout(self): return self.__stdout.strip()

    @property
    def stderr(self): return self.__stderr.strip()

    def is_json(self):
        try:
           return json.loads(self.stdout)
        except:
           return False

    @property
    def json(self):
        json = self.is_json()
        return {} if not json else json


class AzureCLIAdHocExec(CLIExecutor):
    def execute(self, command, subscription=None, **kwargs):
        arg = lambda key, default=None, delete=False: kwargs.pop(key, default) if delete else kwargs.get(key, default)
        cmd = [ "/usr/local/bin/az", ] + command
        if arg("subscription", None): cmd += [ "--subscription", arg("subscription", delete=True) ]
        if arg("location", None): cmd += [ "--location", arg("location", delete=True) ]
        cmd += [ "--output", "json" ]
        return super(AzureCLIAdHocExec, self).execute(cmd, **kwargs)
    pass


class AzureCLIRestAPICall(CLIExecutor):
    def execute(self, command, method="GET", **kwargs):
        cmd = [ "az", "rest", "--method", method.lower(), ] + command
        return super(AzureCLIRestAPICall, self).execute(command, **kwargs)
    pass


class CLICommandFailedRun(Exception):
    def __init__(self, message, command=None, stdout=None, stderr=None):
        self.message = message
        self.stdout = stdout
        self.stderr = stderr
        self.command = command


class AzureClient(object):
    """
    Implements dialogs with Azure to obtain needed details
    """
    def __init__(self, subscription_id=""):
        self.azclient = AzureCLIAdHocExec()
        pass

    @property
    def last_error(self): return self.__last_error

    @property
    def last_error_msg(self):
        return "{0}, command: {1}, with: {3}".format(self.__last_error[0], self.__last_error[1], self.__last_error[2], self.__last_error[3])

    @property
    def json(self): return self.azclient.json

    @property
    def output(self): return self.azclient.stdout


    def do_request(self, **kwargs):
        """
        Returns None if the command failed, then
        check details in last_error.
        Returns a JSON object or Plain text if success
        """
        arg = lambda key, default=None, delete=False: kwargs.pop(key, default) if delete else kwargs.get(key, default)
        err_msg, command = arg("err_msg", delete=True), arg("command", delete=True)
        result, format, stdout, stderr = self.azclient.execute(command, **kwargs)
        if not result:
            self.__last_error = ( err_msg, self.azclient.command, stdout, stderr )
            return None
        self.__last_error = ( None, None, None, None )
        # If the command does not give any output
        # the function will return True
        if format == 'plain' and not len(stdout):
            response = True
        else:
            response = self.azclient.json if format == 'json' else stdout
        return response


    def get_subscription(self, **kwargs):
        arg = lambda key: kwargs.get(key, None)
        return self.do_request(
            err_msg = "Cannot get requested subscription: \"{0}\"".format(arg("subscription")),
            command = [ "account", "show" ],
        )

    def set_subscription(self, **kwargs):
        arg = lambda key: kwargs.get(key, None)
        return self.do_request(
            err_msg = "Cannot set up the needed subscription \"{0}\"".format(arg("subscription")),
            command = [ "account", "set", "--subscription", arg("subscription") ]
        )

    def list_locations(self, **kwargs):
        return self.do_request(
            err_msg = "Cannot obtain a list of Azure regions",
            command = [ "account", "list-locations" ],
            **kwargs
        )

    def list_aro_clusters(self, **kwargs):
        return self.do_request(
            err_msg = "Cannot obtain a list of existing ARO clusters",
            command = [ "openshift", "list" ],
            **kwargs
        )

    def list_providers(self, **kwargs):
        return self.do_request(
            err_msg = "Cannot obtain a list of providers",
            command = [ "provider", "list" ],
            **kwargs
        )

    def register_providers(self, **kwargs):
        for provider in kwargs.get("providers", None):
            res = self.do_request(
                err_msg = "Cannot register provider: \"{0}\"".format(provider),
                command = [ "provider", "register", "--namespace", provider, "--wait" ],
                **kwargs
            )
            if not res:
                print(self.last_error_msg)
                return False
        return True


