#!/usr/bin/env python


import unittest

from azureclient import CLIExecutor
from azureclient import AzureCLIAdHocExec
from azureclient import AzureClient





class TestCLIExecutor(unittest.TestCase):

    account_show = '''{
          "environmentName": "AzureCloud",
          "id": "1ae82641-97e4-4a38-bf96-6a7c1132a6eb",
          "isDefault": true,
          "name": "Pay-As-You-Go",
          "state": "Enabled",
          "tenantId": "00e69474-46dc-4d51-b896-45fc0e789edb",
          "user": {
            "name": "dm150507@mail.ru",
            "type": "user"
          }
        }'''

    def test_simple_prod_01(self):
        run = CLIExecutor()
        result = run.execute([ "ls", "/dev/stdout" ])
        self.assertTrue(result)
        self.assertTrue(run.stdout == "/dev/stdout")
        self.assertFalse(run.stderr == "/dev/stdout")
        self.assertTrue(run.stderr == "")
        pass
    pass


    def test_simple_prod_02(self):
        run = CLIExecutor()
        result = run.execute([ "ls", "/dev/faraway" ], split_stderr=True)
        self.assertFalse(result[0])
        self.assertTrue(run.stdout == "")
        self.assertTrue(run.stderr == "ls: /dev/faraway: No such file or directory")
        pass
    pass


    def test_simple_prod_03(self):
        run = CLIExecutor()
        result = run.execute([ "ls", "/dev/faraway" ], split_stderr=False)
        self.assertFalse(result[0])
        self.assertTrue(run.stdout == "")
        self.assertTrue(run.stderr == "ls: /dev/faraway: No such file or directory")
        pass
    pass

    def test_simple_debug(self):
        run = CLIExecutor(debug_mode=True)
        result = run.execute([ "ls", "/dev/stdout" ], in_output="/dev/stdout", result=True)
        self.assertTrue(result)
        self.assertTrue(run.stdout == "/dev/stdout")
        self.assertFalse(run.stderr == "/dev/stdout")
        self.assertTrue(run.stderr == "")
        pass


    def test_AzureCLIAdHocExec_prod_03(self):
        run = AzureCLIAdHocExec(debug_mode=True)
        result = run.execute([ "account", "show" ], split_stderr=False, in_output=self.account_show)
        self.assertTrue(result[0])
        self.assertTrue(run.stderr == "")
        self.assertTrue(run.json.get("user", {}).get("name") == "dm150507@mail.ru")
        pass
    pass


    def test_AzureCLIAdHocExec_prod_04(self):
        run = AzureCLIAdHocExec(debug_mode=False)
        result = run.execute([ "account", "show" ], split_stderr=False, in_output=self.account_show)
        self.assertTrue(result)
        self.assertTrue(run.stderr == "")
        self.assertTrue(run.json.get("user", {}).get("name") == "dm150507@mail.ru")
        pass
    pass

    def test_AzureClient_prod_01(self):
        client = AzureClient()
        locations = client.list_locations()
        self.assertTrue(len(locations) > 0)
        locations = client.list_locations(query_filter="[*].[name,displayName]")
        self.assertTrue(len(locations) > 0)
        pass
    pass

    def test_AzureClient_prod_02(self):
        subsc_id = "1ae82641-97e4-4a38-bf96-6a7c1132a6eb"
        client = AzureClient(subscription_id = subsc_id)
        locations = client.list_locations()
        pass
    pass

    def test_AzureClient_prod_03(self):
        subsc_id = "1ae82641-97e4-4a38-bf96-6a7c1132a6eb"
        client = AzureClient(subscription_id = subsc_id)
        r = client.get_subscription(subscription = subsc_id)
        self.assertIsNotNone(r)
        pass
    pass


    def test_AzureClient_prod_04(self):
        subsc_id = "1ae82641-97e4-4a38-bf96-6a7c1132a6eb-bad"
        client = AzureClient()
        r = client.set_subscription(subscription = subsc_id)
        self.assertIsNone(r)
        pass


    pass


    def test_AzureClient_prod_05(self):
        client = AzureClient()
        r = client.list_locations(query_filter="[?name=='eastus'].[name, displayName][0]")
        self.assertIn("eastus", client.json)
        pass


    def test_AzureClient_prod_06(self):
        client = AzureClient()
        r = client.do_request(
            attr="test",
            command=[ "account", "show" ],
            query_filter="[name, tenantId]",
        )
        self.assertIsNotNone(r)
        pass


    def test_AzureClient_prod_07(self):
        client = AzureClient()
        r = client.list_aro_clusters()
        self.assertIsNone(r)
        pass


    def test_AzureClient_prod_08(self):
        client = AzureClient()
        r = client.do_request(
            err_msg = "Checking",
            command = [ "nothing", ],
        )
        self.assertIsNone(r)
        pass


    def test_AzureClient_prod_09(self):
        client = AzureClient()
        r = client.list_providers(query_filter = "[?registrationState != 'Registered'].[namespace,registrationState]")
        self.assertTrue(r)
        r = client.register_providers(
            providers = (
                "Microsoft.SecurityInsights",
                "Microsoft.Peering",
            )
        )
        print(r)
        self.assertTrue(r)


    pass
if __name__ == "__main__":
    unittest.main(verbosity=2)
    pass

